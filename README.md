# iTextPDF 5.5.13.3 相关Jar包和源码Jar包

本仓库提供了 `com.itextpdf.itextpdf.5.5.13.3` 版本的Jar包及其包含源码的Jar包。这些资源文件可以帮助你在项目中集成iTextPDF库，以便进行PDF文档的生成和处理。

## 资源文件内容

- **itextpdf-5.5.13.3.jar**: iTextPDF库的核心Jar包。
- **itextpdf-5.5.13.3-sources.jar**: 包含iTextPDF库的源码Jar包，方便开发者进行调试和学习。

## Maven依赖配置

如果你使用Maven构建工具，可以在你的`pom.xml`文件中添加以下依赖配置：

```xml
<dependency>
    <groupId>com.itextpdf</groupId>
    <artifactId>itextpdf</artifactId>
    <version>5.5.13.3</version>
</dependency>
```

## 下载地址

你可以直接从以下地址下载资源文件：

[iTextPDF 5.5.13.3 下载地址](https://github.com/itext/itextpdf/releases/tag/5.5.13.3)

## 打赏支持

如果你觉得这个资源对你有帮助，欢迎打赏1积分支持我们！

---

希望这个资源能够帮助你在项目中顺利集成iTextPDF库，祝你开发顺利！